function! RMarkdownFoldSection()
    let tline = getline(v:lnum)
    let pline = getline(v:lnum - 1)
    let nline = getline(v:lnum + 1)

    " Code chunk
    if match(tline, '\s*```{') >= 0
        return "a1"
    elseif match(tline, '\s*```$') >= 0
        return "s1"
    else
        return call MarkdownFoldSection()
    endif
endfunction

