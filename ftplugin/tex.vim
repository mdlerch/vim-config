function! LatexExit(job_id, data)
    if a:data == 0
        echom 'Latex successful'
    else
        echom 'Latex fail' . string(a:data)
    endif
endfunction

function! Latex(latex)
    let cmd = a:latex . ' ' . expand("%")
    call jobstart(cmd, {'on_exit': 'g:LatexExit'})
endfunction

function! LatexDVI2PDF()
    let cmd = 'latex ' . expand("%") . '; dvipdf' . expand("%:r") . '.dvi'
    call jobstart(cmd, {'on_exit': 'g:LatexExit'})
endfunction


if (&ft == "rnoweb")
    finish
endif

source ~/.vim/ftplugin/document_preview_bindings.vim

noremap <buffer><leader>kd <ESC>:w<CR>:call Latex("texi2pdf")<CR>
inoremap <buffer><leader>kd <ESC>:w<CR>:call Latex("texi2pdf")<CR>
noremap <buffer><leader>kk <ESC>:w<CR>:call Latex("texi2pdf")<CR>
inoremap <buffer><leader>kk <ESC>:w<CR>:call Latex("texi2pdf")<CR>
noremap <buffer><leader>kv <ESC>:w<CR>:call Latex("texi2dvi")<CR>
inoremap <buffer><leader>kv <ESC>:w<CR>:call Latex("texi2dvi")<CR>
noremap <buffer><leader>kb <ESC>:w<CR>:call Latex("bibtex")<CR>
inoremap <buffer><leader>kb <ESC>:w<CR>:call Latex("bibtex")<CR>
noremap <buffer><leader>kc <ESC>:w<CR>:call LatexDVI2PDF()<CR>
inoremap <buffer><leader>kc <ESC>:w<CR>:call LatexDVI2PDF()<CR>

